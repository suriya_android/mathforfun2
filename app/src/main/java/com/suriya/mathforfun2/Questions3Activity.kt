package com.suriya.mathforfun2

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import kotlinx.android.synthetic.main.activity_quest3.*
import kotlin.random.Random

class Questions3Activity : AppCompatActivity() {
    private var mulNum1:Int = 0
    private var mulNum2:Int = 0
    private var summation:Int = 0
    private var mulCorrect:Int = 0
    private var mulIncorrect:Int = 0
    private var finishedSet:Boolean = false
    private var i = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quest3)
        run()
    }
    private fun run(){
        summation = randPlusQuestion()
        i = Random.nextInt(1,3)
        makeButtonAnswer()
        makeButtonIncorrectAnswer()
        buttonsClick()
        displayValueInView()
        intent.putExtra("correct",mulCorrect)
        intent.putExtra("incorrect",mulIncorrect)
        setResult(Activity.RESULT_OK,intent)
    }
    private fun displayValueInView(){
        txt_multiple_num_1.text = mulNum1.toString()
        txt_multiple_num_2.text = mulNum2.toString()
        txt_multiple_correct.text  = "ถูก = $mulCorrect"
        txt_multiple_incorrect.text  = "ไม่ถูก = $mulIncorrect"
    }
    private fun makeButtonAnswer(){
        when(i){
            1 -> btn_multiple_ans1.text = summation.toString()
            2 -> btn_multiple_ans2.text = summation.toString()
            3 -> btn_multiple_ans3.text = summation.toString()
        }
        finishedSet = true
    }
    private fun makeButtonIncorrectAnswer(){
        if(finishedSet){
            if(i ==1){
                btn_multiple_ans2.text = (summation + 1).toString()
                btn_multiple_ans3.text = (summation + 2).toString()
            }
            if(i == 2){
                btn_multiple_ans1.text = (summation - 1).toString()
                btn_multiple_ans3.text = (summation + 1).toString()
            }
            if(i == 3){
                btn_multiple_ans1.text = (summation - 2).toString()
                btn_multiple_ans2.text = (summation - 1).toString()
            }
        }
    }
    private fun buttonsClick(){
        checkAns(btn_multiple_ans1)
        checkAns(btn_multiple_ans2)
        checkAns(btn_multiple_ans3)
    }
    private fun checkAns(btn: Button){
        val intent = Intent()
        btn.setOnClickListener {
            if(btn.text == summation.toString()){
                mulCorrect ++
                txt_multiple_result.text = "ถูกต้อง"
                btn.setBackgroundColor(Color.GREEN)
            }else{
                mulIncorrect ++
                txt_multiple_result.text = "ไม่ถูกต้อง"
                btn.setBackgroundColor(Color.RED)
            }
            Handler().postDelayed({
                clearColor(btn)
                run()
            },250)
        }
    }
    private fun clearColor(btn:Button){
        btn.setBackgroundColor(Color.parseColor("#FF6B778D"))
    }
    private fun randPlusQuestion():Int{
        mulNum1 = Random.nextInt(1,5)
        mulNum2 = Random.nextInt(1,5)
        return if(mulNum1 > mulNum2) {
            mulNum1 * mulNum2
        }else{
            mulNum2 * mulNum1
        }
    }
}