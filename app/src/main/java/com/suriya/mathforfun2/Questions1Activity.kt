package com.suriya.mathforfun2

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_quest1.*
import kotlin.random.Random

class Questions1Activity : AppCompatActivity() {
    private var plusNum1:Int = 0
    private var plusNum2:Int = 0
    private var summation:Int = 0
    private var plusCorrect:Int = 0
    private var plusIncorrect:Int = 0
    private var finishedSet:Boolean = false
    private var i = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quest1)
        run()
    }
    private fun run(){
        summation = randPlusQuestion()
        i =  Random.nextInt(1,3)
        makeButtonAnswer()
        makeButtonIncorrectAnswer()
        buttonsClick()
        displayValueInView()
        intent.putExtra("correct",plusCorrect)
        intent.putExtra("incorrect",plusIncorrect)
        setResult(Activity.RESULT_OK,intent)
    }
    private fun displayValueInView(){
        txt_plus_num_1.text = plusNum1.toString()
        txt_plus_num_2.text = plusNum2.toString()
        txt_plus_correct.text  = "ถูก = $plusCorrect"
        txt_plus_incorrect.text  = "ไม่ถูก = $plusIncorrect"
    }
    private fun makeButtonAnswer(){
        when(i){
            1 -> btn_plus_ans1.text = summation.toString()
            2 -> btn_plus_ans2.text = summation.toString()
            3 -> btn_plus_ans3.text = summation.toString()
        }
        finishedSet = true
    }
    private fun makeButtonIncorrectAnswer(){
        if(finishedSet){
            if(i ==1){
                btn_plus_ans2.text = (summation + 1).toString()
                btn_plus_ans3.text = (summation + 2).toString()
            }
            if(i == 2){
                btn_plus_ans1.text = (summation - 1).toString()
                btn_plus_ans3.text = (summation + 1).toString()
            }
            if(i == 3){
                btn_plus_ans1.text = (summation - 2).toString()
                btn_plus_ans2.text = (summation - 1).toString()
            }
        }
    }
    private fun buttonsClick(){
        checkAns(btn_plus_ans1)
        checkAns(btn_plus_ans2)
        checkAns(btn_plus_ans3)
    }
    private fun checkAns(btn:Button){
        val intent = Intent()
        btn.setOnClickListener {
            if(btn.text == summation.toString()){
                plusCorrect ++
                txt_plus_result.text = "ถูกต้อง"
                btn.setBackgroundColor(Color.GREEN)
            }else{
                txt_plus_result.text = "ไม่ถูกต้อง"
                plusIncorrect ++
                btn.setBackgroundColor(Color.RED)
            }
            Handler().postDelayed({
                clearColor(btn)
                run()
            },250)
        }
    }
    private fun clearColor(btn:Button){
        btn.setBackgroundColor(Color.parseColor("#FF6B778D"))
    }
    private fun randPlusQuestion():Int{
        plusNum1 = Random.nextInt(1,10)
        plusNum2 = Random.nextInt(1,10)
        return plusNum1 + plusNum2
    }
}