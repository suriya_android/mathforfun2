package com.suriya.mathforfun2

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    private val QUESTION_ACTIVITY_CODE = 0
    private var globalCorrect = 0
    private var globalIncorrect = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnQuestion1 = findViewById<Button>(R.id.btn_quest_plus)
        val btnQuestion2 = findViewById<Button>(R.id.btn_quest_minus)
        val btnQuestion3 = findViewById<Button>(R.id.btn_quest_muliple)
        val txtGlobalCorrect = findViewById<TextView>(R.id.txt_glo_cor)
        val txtGlobalIncorrect = findViewById<TextView>(R.id.txt_glo_inc)
        txtGlobalCorrect.text = "ถูก $globalCorrect"
        txtGlobalIncorrect.text = "ไม่ถูก $globalIncorrect"
        btnQuestion1.setOnClickListener {
            val intent = Intent(MainActivity@this,Questions1Activity::class.java)
            startActivityForResult(intent,QUESTION_ACTIVITY_CODE)
        }
        btnQuestion2.setOnClickListener {
            val intent = Intent(applicationContext,Questions2Activity::class.java)
            startActivityForResult(intent,QUESTION_ACTIVITY_CODE)
        }
        btnQuestion3.setOnClickListener {
            val intent = Intent(applicationContext,Questions3Activity::class.java)
            startActivityForResult(intent,QUESTION_ACTIVITY_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val returnCorrect = data!!.getStringExtra("correct")
        if(requestCode == QUESTION_ACTIVITY_CODE){
            if(resultCode == Activity.RESULT_OK){
                val returnCorrect = data!!.getIntExtra("correct",globalCorrect)
                val returnIncorrect = data!!.getIntExtra("incorrect",globalIncorrect)
                    globalCorrect += returnCorrect
                    globalIncorrect += returnIncorrect
                Log.e("RETURN NUMBER CORRECT",returnCorrect.toString())
                Log.e("RETURN NUMBER INCORRECT",returnIncorrect.toString())
                Log.e("GLOBAL NUMBER CORRECT",globalCorrect.toString())
                Log.e("GLOBAL NUMBER INCORRECT",globalIncorrect.toString())
                txt_glo_cor.text = "ถูก $globalCorrect"
                txt_glo_inc.text = "ไม่ถูก $globalIncorrect"
            }
        }
    }
      fun Activity.isConnected(ctx:Context):Boolean{
        val cm = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
          val activeNetwork = cm.activeNetworkInfo
          return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }
}