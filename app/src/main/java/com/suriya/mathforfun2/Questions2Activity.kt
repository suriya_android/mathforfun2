package com.suriya.mathforfun2

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import kotlinx.android.synthetic.main.activity_quest2.*
import kotlin.random.Random

class Questions2Activity : AppCompatActivity() {
    private var minusNum1:Int = 0
    private var minusNum2:Int = 0
    private var summation:Int = 0
    private var minusCorrect:Int = 0
    private var minusIncorrect:Int = 0
    private var finishedSet:Boolean = false
    private var i = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quest2)
        run()
    }
    private fun run(){
        summation = randPlusQuestion()
        i = Random.nextInt(1,3)
        makeButtonAnswer()
        makeButtonIncorrectAnswer()
        buttonsClick()
        displayValueInView()
        intent.putExtra("correct",minusCorrect)
        intent.putExtra("incorrect",minusIncorrect)
        setResult(Activity.RESULT_OK,intent)
    }
    private fun displayValueInView(){
        txt_minus_num_1.text = minusNum1.toString()
        txt_minus_num_2.text = minusNum2.toString()
        txt_minus_correct.text  = "ถูก = $minusCorrect"
        txt_minus_incorrect.text  = "ไม่ถูก = $minusIncorrect"
    }
    private fun makeButtonAnswer(){
        when(i){
            1 -> btn_minus_ans1.text = summation.toString()
            2 -> btn_minus_ans2.text = summation.toString()
            3 -> btn_minus_ans3.text = summation.toString()
        }
        finishedSet = true
    }
    private fun makeButtonIncorrectAnswer(){
        if(finishedSet){
            if(i ==1){
                btn_minus_ans2.text = (summation + 1).toString()
                btn_minus_ans3.text = (summation + 2).toString()
            }
            if(i == 2){
                btn_minus_ans1.text = (summation - 1).toString()
                btn_minus_ans3.text = (summation + 1).toString()
            }
            if(i == 3){
                btn_minus_ans1.text = (summation - 2).toString()
                btn_minus_ans2.text = (summation - 1).toString()
            }
        }
    }
    private fun buttonsClick(){
        checkAns(btn_minus_ans1)
        checkAns(btn_minus_ans2)
        checkAns(btn_minus_ans3)
    }
    private fun checkAns(btn: Button){
        val intent = Intent()
        btn.setOnClickListener {
            if(btn.text == summation.toString()){
                minusCorrect ++
                txt_minus_result.text = "ถูกต้อง"
                btn.setBackgroundColor(Color.GREEN)
            }else{
                minusIncorrect ++
                txt_minus_result.text = "ไม่ถูกต้อง"
                btn.setBackgroundColor(Color.RED)
            }
            Handler().postDelayed({
                clearColor(btn)
                run()
            },250)
        }
    }
    private fun clearColor(btn:Button){
        btn.setBackgroundColor(Color.parseColor("#FF6B778D"))
    }
    private fun randPlusQuestion():Int{
        minusNum1 = Random.nextInt(1,10)
        minusNum2 = Random.nextInt(1,10)
        return if(minusNum1 > minusNum2) {
            minusNum1 - minusNum2
        }else{
            minusNum2 - minusNum1
        }
    }
}